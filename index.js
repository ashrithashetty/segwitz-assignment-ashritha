const express=require("express");
const bodyParser=require('body-parser');
var process = require('process');

var app = express();
const ProdReady = require('./app/util/prodReady'); 
const salesController=require('./app/modules/sales.routes');

ProdReady.make(app); // Production ready REST Services

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

//Registration
app.use('/api/sales/', salesController);

const port = process.env.PORT || 3000;

console.log("Services at port = ", port);
app.listen( port);
