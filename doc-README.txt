// create a Database Sales and table sales in MYSQL using the following querries.

//---------------------- Creating DB ---------------------
CREATE DATABASE sales;


//------------------- Creating table -------------------
CREATE TABLE sales (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255),
    amount VARCHAR(255),
    createddate VARCHAR(255),
    PRIMARY KEY (id)
);


// MySQL Local Server is being used for writing this code, the details can be found in the Config file you can update it to the required servers

================================================================================================
// --------------------------- API's --------------------------- 

1. localhost:3000/api/sales (To get All Data)
2. localhost:3000/api/sales/create (To create Sales Data)
Body : 
{
    "username": "Name6",
    "amount": 213
}
3. localhost:3000/api/sales/daily (To get Daily Average Data Based on the amount)
4. localhost:3000/api/sales/weekly (To get Weekly Average Data Based on the amount)
5. localhost:3000/api/sales/monthly (To get Montly Average Data Based on the amount)
6. localhost:3000/api/sales/yearly (To get Yearly Average Data Based on the amount)