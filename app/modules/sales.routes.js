const express = require('express');
const app = express.Router();
const SalesDAO = require("./sales.dao");
const APIResponse = require("../util/apiResponse");
const SalesModel = require("./sales.model");
const logger = require("../util/logger");

app.get('/', async (req, res) => {
    try {
        const rows = await SalesDAO.getAll();
        APIResponse.sendResponse(res, 200, "Success", rows);
    } catch (e) {
        logger.error("Error", e);
        APIResponse.sendResponse(res, 400, "Failed", e);
    }
});

app.get('/daily', async (req, res) => {
    try {
        const rows = await SalesDAO.getDailyAvgData();
        APIResponse.sendResponse(res, 200, "Success", rows);
    } catch (e) {
        logger.error("Error", e);
        APIResponse.sendResponse(res, 400, "Failed", e);
    }
});

app.get('/weekly', async (req, res) => {
    try {
        const rows = await SalesDAO.getWeeklyAvgData();
        APIResponse.sendResponse(res, 200, "Success", rows);
    } catch (e) {
        logger.error("Error", e);
        APIResponse.sendResponse(res, 400, "Failed", e);
    }
});

app.get('/monthly', async (req, res) => {
    try {
        const rows = await SalesDAO.getMonthlyAvgData();
        APIResponse.sendResponse(res, 200, "Success", rows);
    } catch (e) {
        logger.error("Error", e);
        APIResponse.sendResponse(res, 400, "Failed", e);
    }
});

app.get('/yearly', async (req, res) => {
    try {
        const rows = await SalesDAO.getYearlyAvgData();
        APIResponse.sendResponse(res, 200, "Success", rows);
    } catch (e) {
        logger.error("Error", e);
        APIResponse.sendResponse(res, 400, "Failed", e);
    }
});

app.post('/create', async (req, res) => {
    // const obj = SalesModel.getUpdateObjectFromRequest(req.body);
    const obj = req.body;
    if (!obj) {
        logger.error("Validation Error: Expected Values not found. Sales Model missing", req.body);
        APIResponse.sendResponse(res, 400, "Failed", obj);
        return;
    }
    const model = {
        username: req.body.username,
        amount: req.body.amount,
        createddate: new Date()
    }
    console.log(model);
    try {
        const rows = await SalesDAO.addSales(model);
        if (rows.affectedRows > 0) {
            APIResponse.sendResponse(res, 200, "Success");
        } else {
            throw Error("Failed");
        }
    } catch (e) {
        logger.error("Error", e)
        APIResponse.sendResponse(res, 400, "Failed", e);
    }
});

module.exports = app;