const JOI = require('joi');

class SalesModel {
    constructor(id, username, amount, createddate) {
        this.id = id;
        this.username = username;
        this.amount = amount;
        this.createddate = createddate;
    }
    static getValidationSchema() {
        return {
            id: JOI.number(),
            username: JOI.string().required(),
            amount: JOI.string().required(),
            createddate: JOI.string().required()
        };
    }
    static isValid(obj) {
        if (!obj)
            return false;
        return JOI.validate(obj, SalesModel.getValidationSchema());
    }
    static getObjectFromRequest(reqBody) {
        if (!reqBody) {
            return null;
        }
        const entityType = new SalesModel(reqBody["id"] || null, reqBody["username"] || null, reqBody["amount"] || null, reqBody["createddate"] || null);
        const validationObj = SalesModel.isValid(entityType, SalesModel.getValidationSchema());
        return validationObj && validationObj['error'] ? null : entityType;
    }

    
    static isUpdateValid(obj, validationSchema) {
        if (!obj) {
            return false;
        }
        return JOI.validate(obj, validationSchema);
    }
    static getUpdateObjectFromRequest(reqBody) {
        if (!reqBody) {
            return null;
        }
        const entityType = new SalesModel(reqBody["id"] || null, reqBody["username"] || null, reqBody["amount"] || null, reqBody["createddate"] || null);
        const validObj = SalesModel.isUpdateValid(reqBody, SalesModel.generateDynamicUpdateValidator(reqBody));
        // console.log('validObj = ', validObj['error']);
        return !validObj || !validObj['error'] ? entityType : null;

    }

    static generateDynamicUpdateValidator(reqBody) {
        if (!reqBody) {
            return null;
        }

        const validator = {};
        const props = Object.getOwnPropertyNames(reqBody);
        props.forEach(prop => {
            switch (prop) {
                case "id":
                    validator[`${prop}`] = JOI.number(); break;
                case "username":
                    validator[`${prop}`] = JOI.string().required(); break;
                case "amount":
                    validator[`${prop}`] = JOI.string().required(); break;
                case "createddate":
                    validator[`${prop}`] = JOI.string(); break;
            }
        });
        return validator;
    }
}

module.exports = SalesModel;