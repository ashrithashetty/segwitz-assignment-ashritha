const DBUtil = require("./../util/config");
const dbRef = DBUtil;

class SalesDAO {
    static getAll() {
        const mysqlConnection = dbRef;
        if (!mysqlConnection) {
            console.log("Connection Failed");
            return null;
        }
        return new Promise((res, rej) => {
            mysqlConnection.query('SELECT * FROM sales', (err, rows, fields) => {
                if (err) {
                    rej(err);
                } else {
                    res(rows);
                }
            });
        });
    }

    static addSales(entity) {
        const mysqlConnection = DBUtil
        if (!mysqlConnection) {
            console.log("Connection Failed");
            return null;
        }
        const sql = `INSERT INTO sales SET id = ?, username = ?, amount = ?, createddate = ?`;
        return new Promise((resolve, reject) => {
            mysqlConnection.query(sql, [entity.id, entity.username, entity.amount, entity.createddate], (err, rows, fields) => {
                if (!err) {
                    resolve(rows)
                } else {
                    reject(err);
                }
            });
        })
    }

    static getDailyAvgData() {
        const mysqlConnection = dbRef;
        if (!mysqlConnection) {
            console.log("Connection Failed");
            return null;
        }
        return new Promise((res, rej) => {
            mysqlConnection.query('SELECT CAST(createddate AS DATE), AVG(amount) FROM sales GROUP BY CAST(createddate AS DATE)', (err, rows, fields) => {
                if (err) {
                    rej(err);
                } else {
                    res(rows);
                }
            });
        });
    }

    static getWeeklyAvgData() {
        const mysqlConnection = dbRef;
        if (!mysqlConnection) {
            console.log("Connection Failed");
            return null;
        }
        return new Promise((res, rej) => {
            mysqlConnection.query('SELECT WEEK(createddate),AVG(amount) FROM sales', (err, rows, fields) => {
                if (err) {
                    rej(err);
                } else {
                    res(rows);
                }
            });
        });
    }

    static getMonthlyAvgData() {
        const mysqlConnection = dbRef;
        if (!mysqlConnection) {
            console.log("Connection Failed");
            return null;
        }
        return new Promise((res, rej) => {
            mysqlConnection.query('SELECT MONTH(createddate),AVG(amount) FROM sales GROUP BY YEAR(createddate),MONTH(createddate) ORDER BY YEAR(createddate),MONTH(createddate)', (err, rows, fields) => {
                if (err) {
                    rej(err);
                } else {
                    res(rows);
                }
            });
        });
    }

    static getYearlyAvgData() {
        const mysqlConnection = dbRef;
        if (!mysqlConnection) {
            console.log("Connection Failed");
            return null;
        }
        return new Promise((res, rej) => {
            mysqlConnection.query('SELECT YEAR(createddate),AVG(amount) FROM sales GROUP BY YEAR(createddate) ORDER BY YEAR(createddate)', (err, rows, fields) => {
                if (err) {
                    rej(err);
                } else {
                    res(rows);
                }
            });
        });
    }
}

module.exports = SalesDAO;